const modalFormulario = (idButton, plan, img) => {
    const myModal = new bootstrap.Modal(document.getElementById('modale-planes'))
    document.getElementById('img-banner-modal').setAttribute('src', `${img}`)
    document.getElementById('form-typeForm-modal-planes').value = plan
    let btn = document.getElementsByClassName('btn-formulario-general')
    let btnForm = document.getElementsByClassName(idButton)
    for (let index = 0; index < btn.length; index++) { btn[index].style.display = 'none' }
    for (let index = 0; index < btnForm.length; index++) { btnForm[index].style.display = 'inline-block' }
    myModal.show()
}
const lineaButton = (tel) => { window.location.href = `tel:${tel}` }
const bntDetalles = (clases, btn) => {
    var flip = document.getElementsByClassName(`${clases}`)
    var click = document.getElementsByClassName(`${btn}`)
    if (flip[0].style.display == 'block') {
        for (let index = 0; index < flip.length; index++) { flip[index].style.display = 'none' }
        for (let index = 0; index < click.length; index++) { click[index].innerHTML = '<span class="detalles-icon">+</span>Detalles' }
    } else {
        for (let index = 0; index < flip.length; index++) { flip[index].style.display = 'block' }
        for (let index = 0; index < click.length; index++) { click[index].innerHTML = '<span class="detalles-icon">-</span>Detalles' }
    }
}
const verMasPlanMovil = (clases, btn, classAuto) => {
    var flip = document.getElementsByClassName(`${clases}`)
    var click = document.getElementsByClassName(`${btn}`)
    var classAuto = document.getElementsByClassName(`${classAuto}`)
    if (flip[0].style.display == 'flex') {
        for (let index = 0; index < flip.length; index++) { flip[index].style.display = 'none' }
        for (let index = 0; index < click.length; index++) { click[index].innerHTML = '+ BENEFICIOS' }
        for (let index = 0; index < classAuto.length; index++) { classAuto[index].style.height = '217px' }
    } else {
        for (let index = 0; index < flip.length; index++) { flip[index].style.display = 'flex' }
        for (let index = 0; index < click.length; index++) { click[index].innerHTML = '- BENEFICIOS' }
        for (let index = 0; index < classAuto.length; index++) { classAuto[index].style.height = 'auto'; }
    }
}
const getTime = dateTo => {
    let now = new Date();
    let time = (new Date(dateTo) - now + 1000) / 1000;
    let seconds = ('0' + Math.floor(time % 60)).slice(-2);
    let minutes = ('0' + Math.floor(time / 60 % 60)).slice(-2);
    let hours = ('0' + Math.floor(time / 3600 % 24)).slice(-2);
    let days = Math.floor(time / (3600 * 24));
  
    return {
      seconds,
      minutes,
      hours,
      days,
      time
    };
  };
  
  const countdown = (dateTo, element) => {
    const item = document.getElementById(element);
    const timerUpdate = setInterval(() => {
      let currentTime = getTime(dateTo);
      item.innerHTML = `<h2>${currentTime.days}d : ${currentTime.hours}h : ${currentTime.minutes}m : ${currentTime.seconds}s</h2>`;
  
      if (currentTime.time <= 0) {
        clearInterval(timerUpdate);
        item.innerHTML = `<h2>LA OFERTA HA TERMINADO</h2>`;
      }
    }, 1000);
  };
  
  var new_date1 = new Date();
  new_date1.setDate(30);
  new_date1.setMonth(5);
  new_date1.setFullYear(new Date().getFullYear());
  
  countdown(new_date1, 'countdown1');
  


// Obtener todos los enlaces "Saber más"
const moreInfoLinks = document.querySelectorAll('.more-info');


function toggleDetails(event) {
    event.preventDefault();
    const productDetails = event.currentTarget.closest('.product-card').querySelector('.product-details');
    productDetails.style.display = productDetails.style.display === 'none' ? 'block' : 'none';
}

// Agregar un evento de clic a cada enlace
moreInfoLinks.forEach(link => {
    link.addEventListener('click', (event) => {
        event.preventDefault();
        // Obtener la card correspondiente al enlace
        const card = event.target.closest('.card');
        // Alternar la clase "details-show" en la card
        card.classList.toggle('details-show');
    });
});



// Función para animar los contadores
function animateCounters() {
    // Seleccionar todos los elementos que tienen la clase "counter-number"
    // seleccionamos todos los elementos con la clase "counter-number"
    var counters = document.querySelectorAll(".counter-number");

    // Para cada contador, obtenemos su valor actual y lo incrementamos hasta llegar al valor deseado
    counters.forEach(function (counter) {
        var currentCount = 0;
        var targetCount = parseInt(counter.innerText);
        var interval = setInterval(function () {
            if (currentCount < targetCount) {
                currentCount += 100; // incremento en 100 en lugar de 1 para que se note más rápido
                counter.innerText = currentCount.toLocaleString(); // actualizamos el texto del contador
            } else {
                clearInterval(interval); // detenemos el intervalo cuando llegamos al valor deseado
            }
        }, 10); // ejecutamos el intervalo cada 10 milisegundos
    });
}


function countup() {
    var counters = document.querySelectorAll('.counter-number');
    var speed = 80;
  
    counters.forEach(function(counter) {
      var target = parseFloat(counter.getAttribute('data-target'));
      var count = parseFloat(counter.innerText);
      var increment = Math.ceil(target / (speed / 10));
  
      var timer = setInterval(function() {
        count += increment;
        counter.innerText = count.toLocaleString();
  
        if (count >= target) {
          clearInterval(timer);
          counter.innerText = target.toLocaleString();
        }
      }, 500);
    });
  }
  

// Llamar a la función para animar los contadores cuando se cargue la página
window.addEventListener('load', countup);